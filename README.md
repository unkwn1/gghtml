# gghtml - (Go GET HTML) a threaded html source downloader.

> Speed up the GET request and HTML retrieval.


**What**
> gghtml is a command line program that retrieves the html source code from a list of URLs. 

**Why**
> a Python based newspaper scraping project I was working on had bottlenecked when requiring to fetch hundreds - let alone thousands - of URLs. Instead working around the GIL in python I pass the network concurrency to Go.

> Because some fetches may return sooner than others, ggthml retains their input order and applies it to the JSON struct returned.
## Usage

```py
import subprocess

# a: string = list of urls comma separated
a = "<url1>,<url2>"

return_json = subprocess.run(
  ["go", "<gghtml_DirPath>", a],
  capture_output=True
)
```

### Return

```go
// The return object is a string dumped to 
// stdout in the formatted object below
// Articles (list)
//  |-> webpage (items)

{"Articles": [
  {"InputOrder": int, "html": string}]
}
```

#### TODO

- [ ] updated CLI using [Cobra](https://github.com/spf13/cobra/)