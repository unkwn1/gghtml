package ggthml

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/unkwn1/gghtml/pkg/gghtml"
)

// TODO: output flag (fp) JSON

var version = "0.0.1"

var rootCmd = &cobra.Command{
	Use:     "gghtml",
	Version: version,
	Example: "gghtml '<url1>,<url2>' > output.json",
	Short:   "gghtml - quicky request the html source from many webpages.",
	Args:    cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		i := strings.Split(args[0], ",")
		gghtml.Fetch(i)
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Whoops. There was an error while executing your CLI '%s'", err)
		os.Exit(1)
	}
}
