package main

import (
	ggthml "gitlab.com/unkwn1/gghtml/cmd/gghtml"
)

// main() is the command line entrace to gghtml. Requires 1 string argument.
//
// splits string argument on commas and passing slice to gghtml.Fetch()
func main() {

	ggthml.Execute()

}
